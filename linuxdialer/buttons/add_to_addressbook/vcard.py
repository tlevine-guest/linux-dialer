import vobject


class VCard:
    def __init__(self):
        self.vCard = vobject.vCard()

    def add_name(self, name):
        name = str(name).strip()
        self.vCard.add('fn')
        self.vCard.fn.value = name
        self.__add_n(name)

    def __add_n(self, name):
        name_arr = str(name).split(' ')
        arr_len = len(name_arr)
        given = ''
        family = ''
        additional = ''

        self.vCard.add('n')
        if arr_len is 1:
            given = name_arr[0]
        elif arr_len is 2:
            given = name_arr[0]
            family = name_arr[1]
        else:
            given = name_arr[0]
            additional = name_arr[1]
            family = ' '.join(name_arr[2:])

        self.vCard.n.value = vobject.vcard.Name(family=family, given=given, additional=additional)

    def add_phone(self, phonenumber, type):
        self.vCard.add('tel')
        self.vCard.tel.type_param = type
        self.vCard.tel.value = phonenumber

    def add_email(self, email, type):
        self.vCard.add('email')
        self.vCard.email.type_param = type
        self.vCard.email.value = email

    def add_address(self, street, city, state, postal_code, country, type, extension='', po_box=''):
        self.vCard.add('adr')
        self.vCard.adr.type_param = type
        self.vCard.adr.value = vobject.vcard.Address(street, city, state, postal_code, country, po_box, extension)

    def add_uid(self, uid):
        self.vCard.add('uid')
        self.vCard.uid.value = uid

    def content(self):
        return self.vCard.serialize()

import sqlite3
from pathlib import Path


class ContactsDatabase:

    def initialize(self):
        path = Path('~/.local/share/evolution/addressbook/system/contacts.db').expanduser()
        if not path.is_file():
            return -1
        self.db = sqlite3.connect(str(path.absolute()))

    def generate_new_uid(self):
        cursor = self.db.cursor()
        cursor.execute('SELECT uid FROM folder_id')
        uidlist = []
        for uid in cursor:
            uidlist.append(uid[0])
        if len(uidlist) is 0:
            return 'pas-id-5B10CA9D00000016'
        lastuid = uidlist[len(uidlist) - 1]
        lastuid = int(lastuid[7:], 16)
        newuid_hex = lastuid + 1
        while (True):
            newuid = 'pas-id-' + str(hex(newuid_hex).upper())[2:]
            if newuid not in uidlist:
                break
            newuid_hex = newuid_hex + 1
        return newuid

    def write_into_database(self, uid, phonenumber, email, vcard):
        insert_vcard = "INSERT INTO folder_id(uid, vcard) VALUES('{}','{}')".format(uid, vcard)
        self.db.execute(insert_vcard)

        if phonenumber is not '':
            insert_phonenumber = "INSERT INTO folder_id_phone_list(uid, value) VALUES('{}','{}')".format(uid,
                                                                                                         phonenumber)
            self.db.execute(insert_phonenumber)

        if email is not '':
            insert_email = "INSERT INTO folder_id_email_list(uid, value) VALUES('{}','{}')".format(uid, email)
            self.db.execute(insert_email)

    def close_database(self):
        self.db.commit()
        self.db.close()

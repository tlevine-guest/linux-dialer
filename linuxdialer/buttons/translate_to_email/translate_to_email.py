import sys
from os import system

from PyQt5 import QtCore
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QApplication, QLabel, QPushButton
from database_utils import ContactsDatabase


class Window(QWidget):
    def __init__(self, phonenumber):
        super().__init__()
        self.phonenumber = phonenumber

        self.label = QLabel()
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        close_button = QPushButton('Close')
        close_button.clicked.connect(self.onclick_close)
        self.close_button_hbox = QHBoxLayout()
        self.close_button_hbox.addStretch(1)
        self.close_button_hbox.addWidget(close_button)
        self.close_button_hbox.addStretch(1)

        self.outer_vbox = QVBoxLayout()
        self.outer_vbox.addWidget(self.label)
        self.resize(400, 100)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowMaximizeButtonHint)
        self.setLayout(self.outer_vbox)
        self.setWindowTitle('Translate to Email')

    def get_data(self):
        contactdatabase = ContactsDatabase()
        response = contactdatabase.initialize()
        if response is -1:
            self.label.setText("Gnome Contact database doesn't exists!")
            self.outer_vbox.addLayout(self.close_button_hbox)
        else:
            response = contactdatabase.fetch_data(self.phonenumber)
            if response is -1:
                self.label.setText("Phone Number doesn't exist in addressbook!")
                self.outer_vbox.addLayout(self.close_button_hbox)
            elif response is -2:
                self.label.setText("Contact doesn't has emails!")
                self.outer_vbox.addLayout(self.close_button_hbox)
            else:
                emails = response
                number_of_email = len(emails)
                if number_of_email is 1:
                    self.label.setText('Email found :')
                else:
                    self.label.setText('Emails found :')
                vbox = QVBoxLayout()
                vbox.setSpacing(10)

                for i in range(number_of_email):
                    label = QLabel(emails[i])
                    button = QPushButton('Open')
                    button.clicked.connect(self.onclick_open(emails[i]))
                    hbox = QHBoxLayout()
                    hbox.addWidget(label)
                    hbox.addStretch()
                    hbox.addWidget(button)
                    vbox.addLayout(hbox)
                self.outer_vbox.addLayout(vbox)
        self.outer_vbox.addStretch()

    def onclick_open(self, email):
        def do():
            system('xdg-email ' + email)

        return do

    def onclick_close(self):
        self.close()


app = QApplication(sys.argv)
window = Window(sys.argv[1])
window.show()
window.get_data()
sys.exit(app.exec_())

import sqlite3
from pathlib import Path


class ContactsDatabase:

    def initialize(self):
        path = Path('~/.local/share/evolution/addressbook/system/contacts.db').expanduser()
        if not path.is_file():
            return -1
        self.db = sqlite3.connect(str(path.absolute()))

    def fetch_data(self, phone_number):
        cursor = self.db.cursor()
        cursor.execute("SELECT uid FROM folder_id_phone_list WHERE value ='{}'".format(phone_number))
        self.uid = None
        for uid in cursor:
            self.uid = uid[0]

        if self.uid == None:
            return -1

        cursor.execute("SELECT value FROM folder_id_email_list WHERE uid ='{}'".format(self.uid))
        self.emails = []
        for email in cursor:
            self.emails.append(email[0])

        if len(self.emails) is 0:
            return -2

        return self.emails

    def close_database(self):
        self.db.commit()
        self.db.close()

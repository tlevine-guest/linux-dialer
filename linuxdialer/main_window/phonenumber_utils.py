from phonenumbers import parse, is_valid_number, is_possible_number, format_number
from phonenumbers import geocoder, carrier, timezone, PhoneNumberFormat
import pytz
from datetime import datetime


class PhoneNumber:
    def __init__(self, parsed_phonenumber, location):
        self.phonenumber = parse(parsed_phonenumber, location)

    def country_code(self):
        return self.phonenumber.country_code

    def national_number(self):
        return self.phonenumber.national_number

    def has_leading_zero(self):
        return self.phonenumber.italian_leading_zero

    def is_possible_number(self):
        return is_possible_number(self.phonenumber)

    def is_valid_number(self):
        return is_valid_number(self.phonenumber)

    def format_national(self):
        return format_number(self.phonenumber, PhoneNumberFormat.NATIONAL)

    def format_international(self):
        return format_number(self.phonenumber, PhoneNumberFormat.INTERNATIONAL)

    def location(self):
        return geocoder.description_for_number(self.phonenumber, "en")

    def carrier(self):
        return carrier.name_for_number(self.phonenumber, "en")

    def timezone(self):
        return timezone.time_zones_for_number(self.phonenumber)

    def utc_offset(self):
        offset = datetime.now(pytz.timezone(self.timezone()[0])).strftime('%z')
        return 'UTC'+offset[:3]+':'+offset[3:]

num = PhoneNumber("+17086880299", 'US')

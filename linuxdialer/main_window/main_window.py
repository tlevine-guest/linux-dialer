from PyQt5.QtGui import (QFont)
from PyQt5.QtWidgets import (QWidget, QLabel, QHBoxLayout, QVBoxLayout)

from linuxdialer.main_window.phonenumber_utils import PhoneNumber


class Window(QWidget):

    def __init__(self, phone_number):
        super().__init__()
        default_location = "US"
        try:
            phone_number = PhoneNumber(phone_number, default_location)
        except:
            phone_number = None

        font = QFont()
        font.setPointSize(24)

        self.location = QLabel('Location   : --')
        self.timezone = QLabel('Timezone : --')
        self.carrier = QLabel('Carrier      : --')

        details_vbox1 = QVBoxLayout()
        details_vbox1.addWidget(self.location)
        details_vbox1.addWidget(self.carrier)
        details_vbox1.addStretch(1)

        details_vbox2 = QVBoxLayout()
        details_vbox2.addWidget(self.timezone)
        details_vbox2.addStretch(1)

        details_outer_box = QHBoxLayout()
        details_outer_box.addLayout(details_vbox1)
        details_outer_box.addStretch(1)
        details_outer_box.addLayout(details_vbox2)

        self.display = QLabel()
        self.display.setFont(font)

        phone_number_hbox = QHBoxLayout()
        phone_number_hbox.addStretch(1)
        phone_number_hbox.addWidget(self.display)
        phone_number_hbox.addStretch(1)

        outer_vbox = QVBoxLayout()
        outer_vbox.addLayout(details_outer_box)
        outer_vbox.addLayout(phone_number_hbox)
        outer_vbox.addStretch(1)

        if phone_number is None:
            self.display.setText("Invalid Format")
        elif not phone_number.is_valid_number():
            self.display.setText("Invalid Phone Number")
        else:
            self.display.setText(phone_number.format_national())
            self.location.setText('Location   : ' + phone_number.location())
            self.timezone.setText('Timezone : ' + phone_number.timezone()[0] + ' (' + phone_number.utc_offset() + ")")
            self.carrier.setText(
                'Carrier      : ' + (phone_number.carrier() if phone_number.carrier() != '' else 'Unknown'))
            if not phone_number.is_valid_number():
                self.valid_status.setText("Phone number not valid")

        self.resize(600, 200)
        self.setMinimumWidth(600)
        self.setLayout(outer_vbox)
        self.setWindowTitle("Dialer")

---------------------------------------------------------------------------------
            						                ===========================
            							                    Linux Dialer
            						                ===========================
---------------------------------------------------------------------------------
Week 1
=========
- [x] Requirement gathering and project planning

Week 2
=========

2018-05-21
----------
- [x] Design a basic popup Interface

2018-05-22
----------
- [x] Support tel: input

2018-05-23
----------
 - [x] Phone number validation

2018-05-24
----------
 - [x] Display the phone number in national format
 - [x] Provide error message if phone number is invalid

2018-05-25
-----------
 - [x] Provide some additional info about tel: number

 Week 3
======
2018-05-28
-------
- [x] Read about Gnome Contacts and understand the database tables

2018-05-29
-------
- [x] Write SQL query to add phone number to address book.

2018-05-30
------
- [x] Hardcode query into program

2018-05-31
-----
- [x] Design GUI for input (Contact name, email, Address etc. )

2018-06-01
------
- [x] Complete the GUI design


Week 4
======
2018-06-04
-------
- [x] Read about Gnome Contacts and understand the database tables

2018-06-05
-------
- [x] Write SQL query to fetch email from contact database

2018-06-06
------
- [x] Hardcode query into program

2018-06-07
-----
- [x] Make proper UI for displaying email

2018-06-08
------
- [x] Make error message pop-up of email not found



Week 5
======
2018-06-18
-------
- [ ] Find ISO code for tel: number using country code ( Will be use in truecaller)

2018-06-19
-------
- [ ] Make a data table of country tel: code and country ISO code.

2018-06-20
------
- [ ] Create a HTTP request for truecaller connection, check with proxy network

2018-06-21
-----
- [ ] Start working on "Search on Truecaller"

2018-06-22
------
- [ ] Continue work "Search on Truecaller" and complete it.



Week 6
======

2018-06-25
----------
- [ ] Analysing requirement for button creation framework that run different Python functions

2018-06-26
----------
- [ ] Develop basics of Button Creation Framework

2018-06-27
----------
- [ ] Continue work on Button Creation Framework

2018-06-28
----------
- [ ] Extend the framework to support listeners

2018-06-29
----------
- [ ] Complete the Button Creation Framework


Week 7
=======
2018-07-02
----------
- [ ] Program for command line support

2018-07-03
----------
- [ ] Support multiple phone number through command line

2018-07-04
----------
- [ ] Write program to extract tel: and sip: number from file.

2018-07-05
----------
- [ ] ....... continues and add support in command line

2018-07-06
----------
- [ ] Support vCard file input through command line



Week 8
======
2018-07-09
-------
- [ ] Read about mime handler

2018-07-10
-------
- [ ] Create a mime handler for tel: uri

2018-07-11
------
- [ ] Create a mime handler for sip: uri

2018-07-12
-----
- [ ] Configure  tel: and sip: uri with firefox

2018-07-13
------
- [ ] Testing / Debugging / Documenting


Week 9-12
=====
To be determined...


Task Undetermined
=======
*Call with Signal :-

signal-desktop =  Desktop version doesn't support calling , so it is not feasible to call through signal desktop app.

signal-mobile   = Not feasible


*Call with Skype :-

Can be done easily for old version of skype as it support calling through command line option but new version of skype doesn't. ( Not feasible for new version )

Old Version of skype means : skype that comes with **skype** package name

New Version of skype means : skype that comes with **skypeforlinux** package name

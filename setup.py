#!/usr/bin/env python3
import os

from setuptools import setup


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


requirements = [
    'phonenumbers',
    'pytz',
    'vobject'
]

setup(
    name='linuxdialer',
    version=1.0,
    author='Sanjay Prajapat',
    author_email='sanjaypra555@gmail.com',
    url='https://salsa.debian.org/sanjaypra555-guest/linux-dialer/',
    description='',
    long_description=read('README.md'),
    keywords='linuxdialer',
    platforms='Posix',
    packages=['linuxdialer'],
    install_requires=requirements,
)

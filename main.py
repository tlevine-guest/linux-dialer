#!/usr/bin/env python3
import sys

from PyQt5.QtWidgets import QApplication

from linuxdialer.main_window import main_window

app = QApplication(sys.argv)
window = main_window.Window(sys.argv[1])
window.show()
sys.exit(app.exec_())

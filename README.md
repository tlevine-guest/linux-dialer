# linux-dialer
#### Required Python3 and PyQt5

    git clone https://salsa.debian.org/sanjaypra555-guest/linux-dialer/
    cd linux-dialer
    chmod +x setup.py main.py
    ./setup.py install
    
#### Test main window
    ./main.py tel:+12015550123
    
#### Test 'Add to addressbook' (Button will be added after developing the button creation framework)
    cd linuxdialer/buttons/add_to_addressbook/
    python3 add_to_addressbook.py 9988776655
    
#### Test 'Translate to email' (Button will be added after developing the button creation framework)
    cd linuxdialer/buttons/translate_to_email/
    python3 translate_to_email.py 9988776655
